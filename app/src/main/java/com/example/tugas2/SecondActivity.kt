package com.example.tugas2

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity

class SecondActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        val username: TextView = findViewById(R.id.username)
        val password: TextView = findViewById(R.id.password)

        if(intent != null){
            username.text = "username = "+ intent.getStringExtra("nama");

        }

        if(intent.extras != null){
            val bundle:Bundle = intent.extras!!
            password.text = "password = "+ bundle.getString("sandi");
        }
    }
}
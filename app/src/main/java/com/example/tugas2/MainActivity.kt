package com.example.tugas2

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.tugas2.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.submit.setOnClickListener {
            var intent = Intent(this, SecondActivity::class.java)
            intent.putExtra("nama",binding.username.text.toString())
            val bandel = Bundle()
            bandel.putString("sandi",binding.password.text.toString())
            intent.putExtras(bandel)
            startActivity(intent)
        }
    }
}